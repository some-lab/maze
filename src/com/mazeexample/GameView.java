package com.mazeexample;

import com.mazeexample.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;

public class GameView extends View {
	
	//width and height of the whole maze and width of lines which
	//make the walls
	private int width, height, lineWidth;
	//size of the maze i.e. number of cells in it
	private int mazeSizeX, mazeSizeY;
	//width and height of cells in the maze
	float cellWidth, cellHeight;
	//the following store result of cellWidth+lineWidth 
	//and cellHeight+lineWidth respectively 
	float totalCellWidth, totalCellHeight;
	//the finishing point of the maze
	private int mazeFinishX, mazeFinishY;
	private Maze maze;
	private Game game;
	private Activity context;
	private Paint line, red, background;
	private final Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	Handler RedrawHandler = new Handler(); //so redraw occurs in main thread
	
	public GameView(Context context, Maze maze) {
		super(context);
		this.context = (Activity)context;
		this.maze = maze;
		mazeFinishX = maze.getFinalX();
		mazeFinishY = maze.getFinalY();
		mazeSizeX = maze.getMazeWidth();
		mazeSizeY = maze.getMazeHeight();
		//verticalLines = maze.getVerticalLines();
		//horizontalLines = maze.getHorizontalLines();
		line = new Paint();
		line.setColor(getResources().getColor(R.color.line));
		red = new Paint();
		red.setColor(getResources().getColor(R.color.position));
		background = new Paint();
		background.setColor(getResources().getColor(R.color.game_bg));
		setFocusable(true);
		this.setFocusableInTouchMode(true);
	}
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		width = (w < h)?w:h;
		height = width;         //for now square mazes
		lineWidth = 1;          //for now 1 pixels wide walls
		cellWidth = (width - ((float)mazeSizeX*lineWidth)) / mazeSizeX;
		totalCellWidth = cellWidth+lineWidth;
		cellHeight = (height - ((float)mazeSizeY*lineWidth)) / mazeSizeY;
		totalCellHeight = cellHeight+lineWidth;
		red.setTextSize(cellHeight*0.85f);
		super.onSizeChanged(w, h, oldw, oldh);
	}
	
	protected void onDraw(Canvas canvas) {
		//fill in the background
		canvas.drawRect(0, 0, width, height, background);
		
		boolean[][] hLines = maze.getHorizontalLines();
		boolean[][] vLines = maze.getVerticalLines();
		//iterate over the boolean arrays to draw walls
		for(int i = 0; i < mazeSizeX; i++) {
			for(int j = 0; j < mazeSizeY; j++){
				float x = j * totalCellWidth;
				float y = i * totalCellHeight;
				if(j < mazeSizeX - 1 && vLines[i][j]) {
					//we'll draw a vertical line
					canvas.drawLine(x + cellWidth,y,x + cellWidth,y + cellHeight,line);
				}
				if(i < mazeSizeY - 1 && hLines[i][j]) {
					//we'll draw a horizontal line
					canvas.drawLine(x,y + cellHeight,x + cellWidth,y + cellHeight,line);
				}
			}
			Paint paint = new Paint();
			paint.setStyle(Paint.Style.FILL);
		}
		int currentX = maze.getCurrentX(),currentY = maze.getCurrentY();
		//draw the ball				
		canvas.drawCircle((currentX * totalCellWidth)+(cellWidth/2),   //x of center
				  (currentY * totalCellHeight)+(cellWidth/2),  //y of center
				  (cellWidth*0.45f),                           //radius
				  mPaint);

		//Draw image for ball (pockemon)
		/*Resources res = this.getResources();
		*Bitmap bitmap = BitmapFactory.decodeResource(res, R.drawable.obj);
		*canvas.drawBitmap(bitmap,(currentX * totalCellWidth),(currentY * totalCellHeight),paint);
*/
		//draw the finishing point indicator
		canvas.drawText("fin",
						(mazeFinishX * totalCellWidth)+(cellWidth*0.15f),
						(mazeFinishY * totalCellHeight)+(cellHeight*0.75f),
						red);
		invalidate();
	}

	public void drowRefresh(boolean gameComplete) {
			//the ball was moved so we'll redraw the view
		//RedrawHandler.post(new Runnable() {
		  //  public void run() {	
		    //	invalidate();
		  //}});
			if(gameComplete) {
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setTitle(context.getText(R.string.finished_title));
				LayoutInflater inflater = context.getLayoutInflater();
				View view = inflater.inflate(R.layout.finish, null);
				builder.setView(view);
				View closeButton =view.findViewById(R.id.closeGame);
				closeButton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View clicked) {
						if(clicked.getId() == R.id.closeGame) {
							context.finish();
						}
					}
				});
				AlertDialog finishDialog = builder.create();
				finishDialog.show();
			}
	}
}
