package com.mazeexample;

import com.mazeexample.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class Menu extends Activity implements OnClickListener {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Button newGame = (Button)findViewById(R.id.bNew);
        Button exit = (Button)findViewById(R.id.bExit);
        newGame.setOnClickListener(this);
        exit.setOnClickListener(this);
    }
        public boolean onCreateOptionsMenu(android.view.Menu option) {     
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.layout.option, option);   
         	option.getItem(0).getSubMenu().setHeaderIcon(R.drawable.andro);
         	return true;
        }
     
          public boolean onOptionsItemSelected(MenuItem item) {
             switch (item.getItemId()) {
                case R.id.option:
                   Toast.makeText(Menu.this, "Option", Toast.LENGTH_SHORT).show();
                   return true;
                case R.id.favoris:
                    Toast.makeText(Menu.this, "Favoris", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.stats:
                    Toast.makeText(Menu.this, "Stats", Toast.LENGTH_SHORT).show();
                    return true;
               case R.id.quitter:
                   finish();
                   return true;
             }
         return false; 
         }      
          
	@Override
	public void onClick(View view) {
			switch(view.getId()) {
			case R.id.bExit:
				finish();
				break;
			case R.id.bNew:
				String[] levels = {"Niveau 1", "Niveau 2", "Niveau 3", "Niveau 4"};
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle(getString(R.string.levelSelect));
				builder.setItems(levels, new DialogInterface.OnClickListener() {
				    public void onClick(DialogInterface dialog, int item) {
				    	Intent game = new Intent(Menu.this,Game.class);
						Maze maze = MazeCreator.getMaze(item+1);
						game.putExtra("maze", maze);
						startActivity(game);
				    }
				});
				AlertDialog alert = builder.create();
				alert.show();
		}
	}
}