package com.mazeexample;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;


public class Game extends Activity {
	/** Called when the activity is first created. */
    // Just a RANDOM ID to recognize a Message later 
	protected static final int GUIUPDATEIDENTIFIER = 0x101; 
	
	Thread myRefreshThread = null; 
	SensorManager sensorManager;

	private long lastUpdate = 0;

    float last_x = 0, last_y = 0, last_z = 0;
    /* Our 'ball' is located within this View */ 
    GameView view = null; 
    
    Maze maze = new Maze();
    
    Sensor accSensor;
    SensorEventListener accL;
    
    @Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		Intent intent = getIntent();
		
		Bundle extras = intent.getExtras();
		
		maze = (Maze) extras.get("maze");
		
		view = new GameView(this,maze);
		
		setContentView(view);
		
		//Accelerometre
		sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE); 
		accSensor=sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		accL=new accListener();
	}
	

	
	protected void onResume() 
    { 
      super.onResume(); 
      sensorManager.registerListener(accL, accSensor, SensorManager.SENSOR_DELAY_NORMAL); 
    } 

	protected void onStop() 
    { 
        sensorManager.unregisterListener(accL); 
        super.onStop(); 
    } 
    
	private class accListener implements SensorEventListener 
	{
		@Override
		public void onSensorChanged(SensorEvent event) {
			getAccelerometer(event);
	    }

	    @Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) { }
	}


	public void getAccelerometer(SensorEvent event) 
	{
		Sensor mySensor = event.sensor;
		
	    if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
	        float x = event.values[0];
	        float y = event.values[1];
	        float z = event.values[2];
	        
	        long currentTime = System.currentTimeMillis();
	        
	        if ((currentTime - lastUpdate) > 300) {
	            lastUpdate = currentTime;
	            if (x < last_x)           	
	            		maze.move(view ,Maze.LEFT);
	            else if (x > last_x)
	            		maze.move(view, Maze.RIGHT);

	            if (y < last_y)
	            		maze.move(view, Maze.DOWN);
	            else if (y > last_y)
	            		maze.move(view, Maze.UP);
	        }
	        
	        last_x = x;
            last_y = y;
            last_z = z;
	    }	
	}

}
