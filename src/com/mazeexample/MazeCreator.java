package com.mazeexample;

public class MazeCreator {
	
	public static Maze getMaze(int levelNo) {
		Maze maze = null;
		if(levelNo == 1) {
			maze = new Maze();
				boolean[][] vLines = new boolean[][]{
										{false,false,true ,false,false,false,true ,false,false,false,false,false},
										{false,true ,false,false,false,true ,false,false,false,false,true ,true },
										{true ,false,false,false,false,true ,false,false,false,false,true ,true },
										{true ,true ,false,false,false,true ,true ,true ,false,false,true ,true },
										{true ,true ,true ,false,false,true ,true ,false,true ,false,true ,true },
										{false,true ,true ,true ,false,true ,false,false,false,true ,false,false},
										{false,false,false,true ,false,true ,false,true ,false,false,false,false},
										{false,false,true ,false,true ,false,true ,true ,false,true ,false,false},
										{true ,true ,true ,true ,false,true ,true ,false,false,true ,false,false},
										{false,false,false,true ,false,false,true ,true ,false,true ,true ,false},
										{false,false,true ,false,true ,false,true ,false,false,false,false,false},
										{true ,true ,true ,true ,true ,true ,true ,false,false,true ,false,false},
										{false,false,true ,false,false,true ,false,false,false,false,true ,false}
									 };
				boolean[][] hLines = new boolean[][]{
										{true ,false,false,true ,true ,false,false,false,true ,true ,true ,true ,false},
										{false,true ,true ,true ,true ,true ,true ,true ,true ,true ,false,false,false},
										{false,false,true ,true ,true ,false,false,true ,true ,true ,true ,false,false},
										{false,false,false,true ,true ,true ,false,false,false,true ,false,false,false},
										{false,false,false,false,true ,false,false,true ,true ,true ,false,false,false},
										{true ,true ,false,false,false,true ,true ,true ,true ,false,true ,true ,true },
										{false,true ,true ,true ,true ,true ,false,false,false,true ,true ,true ,false},
										{true ,false,false,false,true ,false,true ,false,true ,false,false,true ,true },
										{false,true ,false,false,false,true ,false,true ,true ,true ,true ,true ,false},
										{true ,true ,false,true ,false,true ,true ,false,false,true ,false,true ,false},
										{false,true ,true ,false,true ,false,false,true ,true ,false,true ,true ,true },
										{false,true ,false,false,true ,false,false,true ,true ,true ,false,false,true }
									};
				maze.setVerticalLines(vLines);
				maze.setHorizontalLines(hLines);
				maze.setStartPosition(0, 0);
				maze.setFinalPosition(12, 12);
			}
		if(levelNo == 2) {
			maze = new Maze();
				boolean[][] vLines = new boolean[][]{
						{false,false,true,false,true,false,false,false,false,false,true,false,false,false,true,false,false},
						{false,true,true,true,false,true,false,false,false,true,true,true,false,true,false,true,false},
						{true,false,false,true,true,false,true,false,false,true,true,true,true,false,false,true,false},
						{false,true,false,false,true,false,true,true,false,true,false,true,true,false,true,true,false},
						{false,true,false,false,false,true,false,true,false,true,true,false,true,true,true,true,true},
						{false,false,false,false,true,false,true,false,true,true,false,true,true,true,true,true,true},
						{false,false,false,false,true,true,false,true,false,true,false,false,true,true,true,false,true},
						{true,false,false,true,false,false,true,false,true,true,true,false,true,true,false,false,true},
						{true,true,true,false,false,true,false,true,false,false,true,false,false,false,true,true,true},
						{true,true,false,true,false,false,true,false,true,false,false,false,false,true,false,true,true},
						{true,true,false,true,false,true,true,true,false,true,true,false,true,false,true,true,true},
						{false,true,false,true,true,true,false,true,false,false,true,false,true,false,false,true,true},
						{true,true,false,true,true,true,true,false,true,false,false,true,false,true,false,false,true},
						{true,false,true,true,false,true,true,true,false,true,false,false,true,true,true,false,true},
						{false,false,false,true,false,true,false,true,false,true,false,true,false,true,false,true,true},
						{true,false,true,false,true,false,false,false,true,false,true,true,true,false,false,true,false},
						{true,false,false,true,false,false,false,true,false,true,true,true,true,true,false,true,false},
						{false,false,false,false,false,false,true,false,false,false,false,true,false,false,true,false,false}
				};
				boolean[][] hLines = new boolean[][]{
						{false,true,false,false,true,false,true,true,true,true,false,false,true,true,false,false,false,true},
						{false,false,true,false,false,true,false,true,true,false,false,false,false,false,true,true,true,false},
						{false,true,true,true,false,true,true,false,true,false,false,false,false,false,true,false,false,true},
						{true,false,false,true,true,false,false,false,true,true,false,true,true,false,false,false,true,false},
						{false,true,true,true,true,true,false,true,false,false,false,false,false,false,false,false,false,false},
						{true,true,true,true,false,false,true,false,true,false,true,true,false,false,false,false,false,false},
						{false,true,true,true,true,true,false,true,false,true,false,true,true,false,false,true,true,false},
						{false,false,false,false,true,false,true,false,true,false,false,false,true,false,true,true,false,false},
						{false,false,false,true,true,true,false,true,false,true,true,true,true,true,false,false,false,false},
						{false,false,true,false,false,true,true,false,true,false,true,false,true,false,true,false,false,false},
						{false,true,false,true,true,false,false,false,false,true,false,true,false,true,false,true,false,false},
						{true,false,true,false,false,false,false,true,true,true,true,false,true,true,true,false,false,false},
						{false,false,false,true,false,false,false,false,false,false,true,true,false,false,false,true,true,false},
						{false,true,true,false,false,true,false,false,true,false,false,true,true,false,true,false,false,false},
						{false,true,false,true,true,false,true,true,true,true,true,false,false,true,false,true,false,false},
						{false,false,true,false,false,true,true,true,false,false,false,false,false,false,true,false,true,false},
						{false,true,true,true,true,true,true,false,true,true,false,false,false,true,false,true,false,true}
				};
				maze.setVerticalLines(vLines);
				maze.setHorizontalLines(hLines);
				maze.setStartPosition(4, 0);
				maze.setFinalPosition(5, 0);
		}
		if(levelNo == 3) {
			maze = new Maze();
				boolean[][] vLines = new boolean[][]{
						{false,false,false,true,false,false,false,false,false,true,false,false,true,false,false,false,true,false,false},
						{false,true,true,false,false,false,false,true,true,true,false,true,false,false,true,true,false,false,false},
						{true,false,false,false,true,false,false,false,true,false,true,false,true,false,false,false,false,false,false},
						{false,true,false,true,false,false,false,true,true,false,false,false,false,true,false,false,true,false,false},
						{true,true,true,false,true,false,true,false,true,false,true,false,false,true,false,true,true,true,false},
						{true,true,true,false,true,false,true,false,true,false,true,false,false,false,false,true,false,false,true},
						{true,false,true,true,false,false,false,true,true,true,false,true,false,false,false,false,false,true,false},
						{true,false,true,true,false,true,true,false,true,false,true,false,true,false,true,true,false,false,true},
						{true,true,true,true,true,true,false,false,false,true,false,true,false,true,true,true,false,true,false},
						{false,true,false,true,true,false,false,false,false,true,false,true,true,true,true,false,false,true,false},
						{true,false,true,false,true,false,false,false,true,false,true,false,true,true,true,true,true,false,false},
						{false,true,true,true,false,true,false,true,false,true,false,true,true,true,false,true,true,false,true},
						{true,true,false,true,false,true,false,false,true,true,false,true,true,false,false,false,false,true,false},
						{true,false,false,true,false,true,false,false,true,false,true,false,true,true,true,false,false,false,false},
						{true,false,true,false,false,true,false,false,false,false,true,false,false,true,true,true,false,false,false},
						{false,false,false,true,true,false,false,false,false,true,true,false,true,false,true,true,true,false,false},
						{false,false,true,false,true,true,false,false,false,true,true,false,false,true,false,true,false,true,false},
						{true,true,false,true,true,true,false,true,true,false,true,false,true,false,false,false,true,false,true},
						{true,false,true,true,true,false,true,true,true,false,false,true,false,true,false,true,true,true,true},
						{false,true,false,false,false,false,false,true,false,false,false,false,false,true,true,false,false,true,false}
									 };
				boolean[][] hLines = new boolean[][]{
						{true,true,true,false,true,true,true,false,true,false,true,false,false,true,false,true,false,true,true,false},
						{false,false,false,true,true,true,true,true,false,false,false,true,true,true,true,false,true,true,true,false},
						{false,true,true,true,false,true,true,true,false,true,true,false,true,false,true,true,true,true,true,true},
						{false,false,false,false,true,true,true,false,false,false,true,true,true,true,false,true,false,false,false,true},
						{false,false,false,true,false,true,false,true,false,true,false,false,true,true,true,false,false,false,true,false},
						{false,false,false,false,true,false,true,false,true,false,true,false,true,true,true,true,true,true,true,false},
						{false,true,true,false,true,true,false,true,false,false,true,false,false,true,true,false,true,true,false,false},
						{false,false,false,false,false,false,false,true,false,true,false,true,true,false,false,false,false,true,true,false},
						{true,false,false,false,false,false,true,true,true,true,true,false,false,true,false,true,true,false,false,true},
						{false,true,true,true,false,true,true,true,true,false,false,true,false,false,false,false,true,true,true,false},
						{false,false,false,false,true,false,true,false,false,true,true,false,false,false,false,false,false,false,true,true},
						{false,true,false,false,false,true,false,true,true,false,true,true,false,false,true,true,false,true,false,false},
						{false,false,true,true,true,false,true,true,false,false,false,false,false,false,false,true,true,true,true,false},
						{false,false,true,false,false,true,false,true,true,true,true,true,true,true,false,false,true,true,true,true},
						{false,true,true,true,false,true,true,true,true,true,false,true,false,false,false,false,false,false,true,true},
						{true,true,true,false,true,false,true,true,true,false,false,false,true,true,true,false,false,true,true,false},
						{false,false,false,true,false,false,true,true,false,true,false,true,true,false,false,true,true,true,false,false},
						{false,false,true,false,false,false,false,false,false,false,true,false,false,true,true,true,false,false,true,false},
						{false,true,false,true,false,true,true,false,false,true,true,true,true,false,false,false,false,false,false,false}
									};
				maze.setVerticalLines(vLines);
				maze.setHorizontalLines(hLines);
				maze.setStartPosition(0, 0);
				maze.setFinalPosition(19, 19);
			}
		if(levelNo == 4) {
			maze = new Maze();
			boolean[][] vLines = new boolean[][]{
					{true,false,true,false,true,false,false,false,true,false,true,false,false,false,false,false,false,true,false,false,false,true,true,true,true,false,false,true,false},
					{true,false,false,false,true,false,true,true,false,false,true,true,true,true,false,true,true,false,true,true,false,false,false,false,true,true,true,false,false},
					{false,false,true,true,false,true,true,false,true,false,false,false,true,false,false,true,false,true,false,true,false,true,false,true,true,false,false,false,true},
					{false,true,false,true,false,true,false,true,false,true,true,false,true,false,true,false,true,false,false,false,true,true,true,false,true,true,false,true,false},
					{true,false,true,false,false,true,false,false,true,false,true,false,false,true,false,true,false,true,false,true,false,true,true,false,true,false,true,false,false},
					{false,false,false,false,true,false,false,false,true,false,false,true,false,false,true,true,true,false,true,true,false,false,true,false,true,false,false,true,false},
					{false,true,true,false,true,true,true,false,false,true,false,true,false,false,false,true,false,true,false,true,false,false,true,false,false,true,false,true,false},
					{true,true,true,true,false,true,true,false,true,true,true,false,true,true,true,false,false,false,true,false,true,false,true,true,false,true,true,false,true},
					{false,true,false,false,true,true,false,false,true,false,true,true,true,false,false,false,true,false,true,true,true,false,true,true,true,true,true,true,true},
					{false,true,true,false,false,true,false,true,false,true,false,true,true,true,true,true,false,true,false,false,true,false,false,true,true,true,false,false,true},
					{true,false,true,false,false,false,true,true,false,true,false,false,true,true,false,true,false,true,false,true,true,true,false,false,true,false,true,false,false},
					{false,false,false,true,false,false,true,false,true,false,true,false,false,true,true,true,true,false,false,true,true,false,false,true,true,true,true,false,false},
					{true,true,true,true,false,false,true,false,false,true,true,true,false,true,true,false,false,true,false,true,true,false,true,true,false,true,false,false,false},
					{true,true,false,false,true,false,true,false,false,false,true,false,false,true,false,false,true,true,true,false,false,true,true,false,false,true,true,true,false},
					{false,false,false,false,false,false,true,false,false,false,false,false,true,false,true,true,false,false,true,true,false,true,false,true,false,true,true,true,false},
					{false,true,true,true,false,true,false,true,true,true,false,false,false,true,false,true,false,false,true,false,false,false,true,true,false,false,false,false,true},
					{false,false,true,false,true,true,true,false,false,true,true,false,false,false,false,false,true,false,true,false,false,true,false,true,true,true,true,true,false},
					{true,false,true,false,false,true,false,true,true,false,true,true,false,true,false,true,false,false,false,true,true,true,true,false,true,false,false,false,true},
					{false,false,true,false,true,true,false,false,false,false,true,false,true,false,true,false,false,true,false,false,false,false,true,true,false,true,false,true,false},
					{true,false,false,true,false,false,false,true,true,false,true,false,true,true,true,true,false,false,true,false,false,false,false,false,true,false,false,false,false},
					{false,true,true,true,false,false,true,true,false,false,true,false,false,true,false,false,true,true,true,true,false,false,true,false,false,false,true,true,true},
					{false,true,true,false,true,true,true,true,true,false,true,false,false,false,true,true,true,true,false,false,true,false,true,false,false,false,true,true,false},
					{true,true,true,true,true,true,true,true,false,true,true,false,true,true,true,false,false,true,false,false,true,true,false,true,true,true,true,true,false},
					{true,true,true,false,false,true,false,true,false,false,true,false,false,true,true,false,true,false,true,false,true,true,false,false,false,true,true,true,false},
					{true,true,true,true,true,false,true,false,false,false,false,true,true,false,false,true,true,false,true,false,true,false,true,true,true,false,true,false,true,true},
					{false,true,false,true,true,false,true,true,true,true,false,false,false,false,true,true,false,true,false,false,true,false,true,false,false,false,true,false,false},
					{true,true,true,false,true,false,false,true,true,false,true,true,false,false,true,true,false,true,false,true,false,true,false,false,false,true,true,true,true},
					{true,false,false,false,true,false,false,false,true,true,false,false,false,true,true,true,false,true,true,true,true,true,true,true,false,false,false,false,false},
					{false,true,false,false,true,false,false,false,true,false,false,true,false,true,true,false,true,true,true,false,true,true,false,true,true,false,true,false,false},
					{false,true,false,false,false,true,false,true,false,true,false,false,true,false,false,false,true,false,true,false,false,true,true,false,false,true,false,true,true}
								 };
			boolean[][] hLines = new boolean[][]{
					{false,true,false,true,false,false,true,false,true,true,false,false,false,true,true,false,true,false,false,false,true,true,false,false,false,true,false,false,true,false},
					{false,false,true,true,true,true,false,false,true,true,false,true,false,false,true,true,false,false,true,false,true,true,true,true,false,false,true,true,true,false},
					{false,true,false,false,true,false,false,true,false,false,true,true,false,false,true,true,true,true,true,false,false,false,false,false,false,false,true,true,false,false},
					{true,false,true,false,false,true,true,false,true,true,false,true,true,false,false,false,false,false,false,true,true,false,false,false,true,false,false,false,true,true},
					{false,false,true,true,true,false,true,true,false,false,true,false,true,true,true,false,true,true,false,false,false,true,false,true,true,true,true,true,true,false},
					{false,true,false,true,true,false,false,true,true,true,true,false,false,true,true,true,false,true,true,true,false,true,true,false,true,false,true,false,false,true},
					{false,true,true,false,false,true,true,false,true,false,false,false,true,false,false,true,false,true,false,false,false,false,true,true,true,false,false,true,false,true},
					{false,false,false,true,true,false,false,true,true,false,true,false,false,false,true,true,true,true,true,true,false,true,false,false,false,false,false,false,true,false},
					{false,true,true,false,true,false,false,true,false,false,false,true,false,false,false,true,true,true,false,false,true,false,true,false,false,false,false,false,false,false},
					{false,true,false,true,false,true,false,true,true,false,false,true,false,false,false,false,true,false,false,true,true,false,true,true,false,false,true,true,false,false},
					{true,false,true,true,true,true,true,false,false,false,true,true,true,false,false,false,true,false,false,true,false,false,false,true,false,true,false,true,true,false},
					{false,false,false,false,true,true,false,false,true,true,false,true,false,true,false,true,false,true,false,true,false,true,true,true,true,false,true,false,true,true},
					{true,false,false,true,false,true,true,true,true,true,false,false,true,false,false,false,true,false,true,false,false,false,false,false,true,false,false,false,true,false},
					{false,true,false,true,true,true,false,true,true,true,false,true,false,true,true,false,false,true,false,true,true,false,false,false,true,true,false,false,false,true},
					{true,true,false,false,true,true,true,true,false,false,true,true,true,false,false,false,true,true,true,false,false,true,true,false,true,false,true,true,true,false},
					{false,true,false,false,false,false,true,false,false,true,false,true,true,true,true,true,false,true,false,true,true,true,true,false,false,false,true,true,false,false},
					{true,true,false,true,true,false,false,true,true,true,false,false,true,true,true,false,true,true,true,true,true,false,false,false,false,false,false,false,true,false},
					{false,true,false,false,true,true,true,false,false,true,false,false,false,false,true,false,true,false,false,true,false,false,false,false,true,true,true,false,true,false},
					{false,true,true,true,false,false,true,false,false,true,false,true,false,false,false,true,true,true,false,true,true,true,true,true,false,false,false,true,true,true},
					{false,false,false,false,true,true,true,false,true,true,false,true,true,true,false,false,true,false,true,true,true,true,false,true,true,true,true,true,true,true},
					{true,false,true,false,false,false,true,true,true,true,true,true,true,false,true,true,false,false,false,false,false,false,true,true,false,true,true,false,false,false},
					{false,true,false,false,false,false,false,false,false,false,false,true,false,true,false,false,false,true,true,true,true,false,false,false,false,true,false,false,false,true},
					{false,false,false,true,true,false,false,false,true,true,false,false,true,false,true,false,true,true,false,true,false,false,true,true,true,false,false,false,false,true},
					{false,false,false,false,true,true,false,true,false,true,true,true,true,false,false,false,true,false,true,false,true,false,false,true,true,true,false,false,false,false},
					{false,false,false,false,false,false,false,true,false,true,false,false,false,true,true,false,false,true,false,true,false,true,false,false,true,false,false,false,false,true},
					{true,true,false,true,false,true,true,false,false,false,true,false,true,true,false,false,true,false,true,true,true,false,false,true,true,true,true,false,false,true},
					{false,false,true,false,true,false,true,false,true,false,true,true,true,true,true,false,false,true,false,false,true,false,false,true,true,true,false,true,true,false},
					{false,false,true,true,false,true,true,true,false,false,true,true,false,true,false,false,true,false,false,false,false,false,false,false,false,true,false,false,true,true},
					{true,false,true,false,true,false,false,true,false,true,false,true,false,false,false,true,false,false,false,false,true,false,false,false,false,true,false,true,false,false}
								};
			maze.setVerticalLines(vLines);
			maze.setHorizontalLines(hLines);
			maze.setStartPosition(0, 7);
			maze.setFinalPosition(29, 26);
		}
		return maze;
	}
}
